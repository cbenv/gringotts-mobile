(function () {

    "use strict";

    var app = angular.module('Gringotts', ['ionic', 'ngCordova', 'ngResource']);

    app.run(function ($ionicPlatform) {

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    });

    app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider.state('main', {
            url: '/main',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'MainController'
        });

        $stateProvider.state('portal', {
            url: '/portal?action',
            templateUrl: 'templates/portal.html',
            controller: 'PortalController'
        })

        $stateProvider.state('main.vault', {
            url: '/vault',
            views: {
                'content': {
                    templateUrl: 'templates/vault.html',
                    controller: 'VaultController'
                }
            }
        });
        
        $stateProvider.state('main.siteView', {
            url: '/vault/:siteID/view',
            views: {
                'content': {
                    templateUrl: 'templates/site-view.html',
                    controller: 'SiteViewController'
                }
            }
        });
        
        $stateProvider.state('main.siteEdit', {
            url: '/vault/:siteID/edit?action',
            views: {
                'content': {
                    templateUrl: 'templates/site-edit.html',
                    controller: 'SiteEditController'
                }
            }
        });
        
        $stateProvider.state('main.cardView', {
            url: '/vault/:cardID/view',
            views: {
                'content': {
                    templateUrl: 'templates/card-view.html',
                    controller: 'CardViewController'
                }
            }
        });
        
        $stateProvider.state('main.cardEdit', {
            url: '/vault/:cardID/edit?action',
            views: {
                'content': {
                    templateUrl: 'templates/card-edit.html',
                    controller: 'CardEditController'
                }
            }
        });
        
        $stateProvider.state('main.passwordGenerator', {
            url: '/password-generator',
            views: {
                'content': {
                    templateUrl: 'templates/password-generator.html',
                    controller: 'PasswordGeneratorController'
                }
            }
        });

        $urlRouterProvider.otherwise('/portal?action=Login');
        
        $httpProvider.defaults.withCredentials = true;
    });

})();