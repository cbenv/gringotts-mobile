(function () {

    "use strict";

    var app = angular.module('Gringotts');

    app.controller('MainController', function ($scope, $state, $log, UserService) {

        $scope.logout = function () {
            $log.info('Logging out');
            function onSuccess(response) {
                $log.info('Logout succeeded');
                $log.debug(response);
                $log.info('Going to portal');
                $state.go('portal', { action: 'Login' });
            }
            function onFailure(response) {
                $log.warn('Logout failed');
                $log.debug(response);
            }
            UserService.logout().then(onSuccess, onFailure);
        };

        $scope.menu = [
            {
                title: 'Vault',
                onClick: function () {
                    $log.info('Going to main.vault');
                    $state.go('main.vault');
                }
            },
            {
                title: 'Password Generator',
                onClick: function () {
                    $log.info('Going to main.passwordGenerator');
                    $state.go('main.passwordGenerator');
                },
            },
            {
                title: 'Sign Out',
                onClick: $scope.logout
            }
        ];
    });

    app.controller('PortalController', function ($scope, $stateParams, $state, $log, UserService) {

        function beforeEnter() {
            $log.info('Getting current user');
            $scope.message = '';
            $scope.action = $stateParams.action || 'Login';
            $scope.credentials = {
                username: '',
                password: '',
                passwordConfirmation: '',
                email: ''
            };
            function onSuccess(response) {
                $log.debug(response);
                $scope.credentials.username = response.username;
            }
            function onFailure(response) {
                $log.debug(response);
            }
            UserService.me().then(onSuccess, onFailure);
        }

        $scope.submit = function (credentials, action) {

            function login(credentials) {
                $log.info('Logging in');
                function onSuccess(response) {
                    $log.info('Login succeeded');
                    $log.debug(response);
                    sessionStorage.setItem('masterpassword', credentials.password)
                    sessionStorage.setItem('userID', response.uid);
                    $log.info('Going to Vault');
                    $state.go('main.vault');
                }
                function onFailure(response) {
                    $log.warn('Login failed');
                    $log.debug(response);
                    $scope.message = parseError(response);
                }
                UserService.login(credentials).then(onSuccess, onFailure);
            }

            function register(credentials) {
                $log.info('Signing up');
                function onSuccess(response) {
                    $log.info('Register succeeded');
                    $log.debug(response);
                    login(credentials);
                }
                function onFailure(response) {
                    $log.warn('Register failed');
                    $log.debug(response);
                    $scope.message = parseError(response);
                }
                if (!credentials.email) {
                    $scope.message = 'Invalid or missing Email';
                }
                else if (credentials.password != credentials.passwordConfirmation) {
                    $scope.message = 'Passwords do not match';
                }
                else {
                    UserService.register(credentials).then(onSuccess, onFailure);
                }
            }

            function recover(credentials) {
                $log.info('Recovering password');
            }

            function parseError(error) {
                var message = error.statusText;
                return message;
            }

            $scope.message = '';
            switch (action) {
                case 'Login':
                    login(credentials);
                    break;
                case 'Register':
                    register(credentials);
                    break;
                case 'Recover':
                    register(credentials);
                    break;
                default:
                    $log.warn('Unknown action:', action);
                    break;
            }
        };

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
    });

    app.controller('VaultController', function ($scope, $state, $log, SiteService, CardService) {

        function fetchSites() {
            $log.info('Fetching sites');
            function onSuccess(response) {
                $log.info('Fetching succeeded');
                $log.debug(response);
                $scope.sites = response;
            }
            function onFailure(response) {
                $log.warn('Fetching failed');
                $log.debug(response);
                $scope.sites = [];
            }
            function onCompletion() {
                $scope.$broadcast('scroll.refreshComplete');
            }
            SiteService.fetchAll().then(onSuccess, onFailure, onCompletion);
        }

        function fetchCards() {
            $log.info('Fetching cards');
            function onSuccess(response) {
                $log.info('Fetching succeeded');
                $log.debug(response);
                $scope.cards = response;
            }
            function onFailure(response) {
                $log.warn('Fetching failed');
                $log.debug(response);
                $scope.cards = [];
            }
            function onCompletion() {
                $scope.$broadcast('scroll.refreshComplete');
            }
            CardService.fetchAll().then(onSuccess, onFailure, onCompletion);
        }

        function beforeEnter() {
            $log.info('Resetting current site and current card');
            SiteService.resetCurrent();
            CardService.resetCurrent();
            fetchSites();
            fetchCards();
            $scope.activeTab = 'sites';
        }

        $scope.openSite = function (site) {
            $log.info('Viewing site', site.name);
            SiteService.setCurrent(site);
            $log.info('Going to main.siteView');
            $state.go('main.siteView');
        };

        $scope.addSite = function () {
            $log.info('Adding new site');
            SiteService.resetCurrent();
            $log.info('Going to main.siteEdit');
            $state.go('main.siteEdit', { action: 'create' });
        };

        $scope.openCard = function (card) {
            $log.info('Viewing card', card.name);
            CardService.setCurrent(card);
            $log.info('Going to main.cardView');
            $state.go('main.cardView');
        };

        $scope.addCard = function () {
            $log.info('Adding new card');
            CardService.resetCurrent();
            $log.info('Going to main.cardEdit');
            $state.go('main.cardEdit', { action: 'create' });
        };

        $scope.add = function () {
            ($scope.activeTab == 'sites') ? $scope.addSite() : $scope.addCard();
        };

        $scope.selectTab = function (tab) {
            $scope.activeTab = tab;
        };

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
    });

    app.controller('SiteViewController', function ($scope, $state, $interval, $log, $cordovaClipboard, SiteService, EncryptionService, TokenGenerator) {

        function beforeEnter() {
            $log.info('Getting current site');
            var esite = SiteService.getCurrent();
            $log.debug(esite);
            if (esite.data.ct) {
                $log.info('Decrypting site');
                $scope.site = {
                    user: esite.user,
                    name: esite.name,
                    id: esite.id,
                    data: EncryptionService.decrypt(esite.data)
                };
            }
            else {
                $scope.site = esite;
            }
            $log.debug($scope.site);
            enter();
        }

        function generateOTP() {
            $log.info('Generating token');
            var counter = Math.floor((new Date() - 0) / $scope.progressMax);
            $scope.progressValue = Math.floor((new Date() - 0) % $scope.progressMax);
            $scope.tokenCode = TokenGenerator.generate($scope.site.data.token.algorithm, $scope.site.data.token.secret, counter, $scope.site.data.token.digits);
        }

        function enter() {
            if ($scope.site.data.token.secret) {
                if ($scope.site.data.token.type == 'TOTP') {
                    $scope.progressMax = ($scope.site.data.token.period * 1000);
                    $log.info('Starting token generation');
                    $scope.interval = $interval(generateOTP, 16);
                }
                else {
                    $scope.tokenCode = TokenGenerator.generate($scope.site.data.token.algorithm, $scope.site.data.token.secret, $scope.site.data.token.counter, $scope.site.data.token.digits);
                }
            }
        }

        function afterLeave() {
            $log.info('Stopping token generation');
            $interval.cancel($scope.interval);
        }

        $scope.editSite = function (site) {
            $log.info('Editing site');
            SiteService.setCurrent(site);
            $log.info('Going to main.siteEdit');
            $state.go('main.siteEdit', { action: 'update' });
        };

        $scope.tickCounter = function () {
            $log.info('Updating counter');
            $scope.site.data.token.counter++;
            generateOTP();
            $log.info('Saving site');
            function onSuccess(response) {
                $log.info('Save succeeded');
                $log.debug(response);
                SiteService.setCurrent(response);
                $log.info('Going to main.siteView');
                $state.go('main.siteView');
            }
            function onFailure(response) {
                $log.warn('Save failed');
                $log.debug(response);
            }
            $log.info('Encrypting site');
            $log.debug($scope.site);
            var esite = {
                user: sessionStorage.getItem('userID'),
                name: $scope.site.name,
                id: $scope.site.id,
                data: EncryptionService.encrypt($scope.site.data)
            }
            SiteService.update(esite.id, esite).then(onSuccess, onFailure);
        };

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
        $scope.$on('$ionicView.afterLeave', afterLeave);
    });

    app.controller('SiteEditController', function ($scope, $state, $stateParams, $log, $cordovaClipboard, $cordovaBarcodeScanner, SiteService, EncryptionService) {

        function beforeEnter() {
            $log.info('Getting current site');
            $scope.site = angular.copy(SiteService.getCurrent());
            $log.debug($scope.site);
            $log.info('Determining action');
            $scope.action = $stateParams.action;
            $log.info('Action: ' + $scope.action);
        }

        $scope.saveSite = function (site) {
            $log.info('Saving site');
            function onSuccess(response) {
                $log.info('Save succeeded');
                $log.debug(response);
                SiteService.setCurrent(response);
                $log.info('Going to main.siteView');
                $state.go('main.siteView');
            }
            function onFailure(response) {
                $log.warn('Save failed');
                $log.debug(response);
            }
            $log.info('Encrypting site');
            $log.debug(site);
            var esite = {
                user: sessionStorage.getItem('userID'),
                name: site.name,
                id: site.id,
                data: EncryptionService.encrypt(site.data)
            }
            var promise = ($scope.action == 'update') ? SiteService.update(esite.id, esite) : SiteService.create(esite);
            promise.then(onSuccess, onFailure);
        };

        $scope.deleteSite = function (site) {
            $log.info('Deleting site');
            function onSuccess(response) {
                $log.info('Delete succeeded');
                $log.debug(response);
                $log.info('Going to main.vault');
                $state.go('main.vault');
            }
            function onFailure(response) {
                $log.warn('Delete failed');
                $log.debug(response);
            }
            SiteService.remove(site.id).then(onSuccess, onFailure);
        };

        $scope.scanQR = function () {
            $log.info('Scanning QR Code');
            function onSuccess(response) {
                $log.info('Scan succeeded');
                var URI = response.text;
                $log.debug(URI);
                var parser = angular.element('a');
                parser.href = URI;
                if (parser.protocol == 'otpauth:') {
                    var token = {};
                    token.type = parser.hostname.toUpperCase();
                    var re = /([^?=&]*)=([^?=&]*)/g;
                    var match = [];
                    while (match = re.exec(parser.search)) {
                        switch (match[1]) {
                            case ('secret'):
                                token.secret = match[2];
                                break;
                            case ('algorithm'):
                                token.algorithm = match[2];
                                break;
                            case ('digits'):
                                token.digits = parseInt(match[2]);
                                break;
                            case ('counter'):
                                token.counter = parseInt(match[2]);
                                break;
                            case ('period'):
                                token.period = parseInt(match[2]);
                                break;
                            default:
                                break;
                        }
                    }
                    token.enabled = true;
                    $log.info('Token parsed');
                    $log.debug(token);
                    $scope.site.data.token = token;
                }
                else {
                    $log.warn('Invalid code: ' + URI);
                    $log.debug(response);
                }
            }
            function onFailure(response) {
                $log.warn('Unrecognized code');
                $log.debug(response);
            }
            $cordovaBarcodeScanner.scan().then(onSuccess, onFailure);
        }

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
    });

    app.controller('CardViewController', function ($scope, $state, $interval, $log, $cordovaClipboard, CardService, EncryptionService) {

        function beforeEnter() {
            $log.info('Getting current card');
            var ecard = CardService.getCurrent();
            $log.debug(ecard);
            if (ecard.data.ct) {
                $log.info('Decrypting card');
                $scope.card = {
                    user: ecard.user,
                    name: ecard.name,
                    id: ecard.id,
                    data: EncryptionService.decrypt(ecard.data)
                };
            }
            else {
                $scope.card = ecard;
            }
            $log.debug($scope.card);
            $log.info('Parsing card');
            var number = '' + $scope.card.data.number;
            if (number.length == 16) {
                number = number.slice(0,4) + ' ' + number.slice(4,8) + ' ' + number.slice(8,12) + ' ' + number.slice(12,16);
            }
            else if (number.length == 15) {
                number = number.slice(0,4) + ' ' + number.slice(4,10) + ' ' + number.slice(10, 15);
            }
            $scope.card.data.eznumber = number;
            var month = ('0' + $scope.card.data.expiration.month).slice(-2);
            var year = ('' + $scope.card.data.expiration.year).slice(-2);
            $scope.card.data.goodthru = month + '/' + year; 
            var address1 = $scope.card.data.billing.address1;
            var address2 = $scope.card.data.billing.address2;
            var address3 = $scope.card.data.billing.city + ', ' + $scope.card.data.billing.state + ' ' + $scope.card.data.billing.zip;
            var address = (address1 + '\n' + address2).trim() + '\n' + address3;
            $scope.card.data.address = address; 
            $scope.rows = address.split('\n').length;
        }

        $scope.editCard = function (card) {
            $log.info('Editing card');
            CardService.setCurrent(card);
            $log.info('Going to main.cardEdit');
            $state.go('main.cardEdit', { action: 'update' });
        };

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
    });

    app.controller('CardEditController', function ($scope, $state, $stateParams, $log, $cordovaClipboard, CardService, EncryptionService) {

        function beforeEnter() {
            $log.info('Getting current card');
            $scope.card = angular.copy(CardService.getCurrent());
            $log.debug($scope.card);
            $log.info('Determining action');
            $scope.action = $stateParams.action;
            $log.info('Action: ' + $scope.action);
        }

        $scope.saveCard = function (card) {
            $log.info('Saving card');
            function onSuccess(response) {
                $log.info('Save succeeded');
                $log.debug(response);
                CardService.setCurrent(response);
                $log.info('Going to main.cardView');
                $state.go('main.cardView');
            }
            function onFailure(response) {
                $log.warn('Save failed');
                $log.debug(response);
            }
            $log.info('Encrypting card');
            var ecard = {
                user: sessionStorage.getItem('userID'),
                name: card.name,
                id: card.id,
                data: EncryptionService.encrypt(card.data)
            }
            var promise = ($scope.action == 'update') ? CardService.update(ecard.id, ecard) : CardService.create(ecard);
            promise.then(onSuccess, onFailure);
        };

        $scope.deleteCard = function (card) {
            $log.info('Deleting card');
            function onSuccess(response) {
                $log.info('Delete succeeded');
                $log.debug(response);
                $log.info('Going to main.vault');
                $state.go('main.vault');
            }
            function onFailure(response) {
                $log.warn('Delete failed');
                $log.debug(response);
            }
            CardService.remove(card.id).then(onSuccess, onFailure);
        };

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
    });

    app.controller('PasswordGeneratorController', function ($scope, $log, $cordovaClipboard, $cordovaToast, PasswordGenerator) {

        function beforeEnter() {
            $log.info('Getting password generator configuration');
            $scope.configuration = PasswordGenerator.getConfiguration();
            $log.debug($scope.configuration);
            $scope.generate();
        }

        $scope.generate = function () {
            $scope.message = '';
            if ($scope.configuration.useLowercase || $scope.configuration.useUppercase || $scope.configuration.useNumeric || $scope.configuration.useSpecial) {
                $log.info('Saving configuration');
                PasswordGenerator.setConfiguration($scope.configuration);
                $log.info('Generating password');
                $scope.password = PasswordGenerator.generate();
            }
            else {
                $scope.message = 'Please choose at least one character pool';
            }
        };

        $scope.copy = function (password) {
            $log.info('Copying generated password');
            $log.debug(password);
            function onSuccess() {
                $log.info('Copy succeeded');
                $cordovaToast.show(password, 'long', 'bottom');
            }
            function onFailure() {
                $log.warn('Copy failed');
            }
            $cordovaClipboard.copy(password).then(onSuccess, onFailure);
        };

        $scope.$on('$ionicView.beforeEnter', beforeEnter);
    });

})();