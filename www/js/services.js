(function () {

    "use strict";

    var app = angular.module('Gringotts');
    
    var endpoint = 'http://loq.exquisense.com';

    app.service('UserService', function ($resource) {

        var service = {
            me: me,
            login: login,
            register: register,
            // recover: recover,
            logout: logout
        };
        
        var url = endpoint + '/user';
        var actions = {
            'me': {
                method: 'GET',
                url: url + '/me',
                withCredentials: true,
            },
            'login': {
                method: 'POST',
                url: url + '/login'
            },
            'register': {
                method: 'POST'
            },
            'logout': {
                method: 'POST',
                url: url + '/logout',
                withCredentials: true
            }
        };
        var resource = $resource(url, {}, actions);

        function me() {
            return resource.me().$promise;
        }
        function login(data) {
            return resource.login(data).$promise;
        }
        function register(data) {
            return resource.register(data).$promise;
        }
        function recover(data) {
            console.log('TO BE IMPLEMENTED');
            return resource.recover(data).$promise;
        }
        function logout() {
            return resource.logout().$promise;
        }

        return service;
    });
    
    app.service('CardService', function ($resource) {
        
        var service = {
            create: create,
            fetchOne: fetchOne,
            fetchAll: fetchAll,
            update: update,
            remove: remove,
            getCurrent: getCurrent,
            setCurrent: setCurrent,
            resetCurrent: resetCurrent,
        }
        
        var current = null;
        var url = endpoint + '/card/:cardID';
        var paramDefaults = {cardID: '@cardID'};
        var userID = sessionStorage.getItem('userID');
        var params = {user: userID};
        var actions = {
            'save': {
                method: 'POST',
                withCredentials: true
            },
            'get': {
                method: 'GET',
                withCredentials: true
            },
            'getAll': {
                method: 'GET',
                params: params,
                isArray: true,
                withCredentials: true
            },
            'update': {
                method: 'PUT',
                withCredentials: true
            },
            'remove': {
                method: 'DELETE',
                withCredentials: true
            }
        };
        var resource = $resource(url, paramDefaults, actions);
        
        function create(card) {
            return resource.save(card).$promise;
        }   
        function fetchOne(cardID) {
            return resource.get({cardID: cardID}).$promise;
        }
        function fetchAll() {
            return resource.getAll().$promise;
        }
        function update(cardID, card) {
            return resource.update({cardID: cardID}, card).$promise;
        }
        function remove(cardID) {
            return resource.remove({cardID: cardID}).$promise;
        }
        function getCurrent() {
            return current;
        }
        function setCurrent(card) {
            current = card;
        }
        function resetCurrent() {
            current = {
                name: '',
                data: {
                    name: '',
                    billing: {
                        address1: '',
                        address2: '',
                        city: '',
                        state: '',
                        zip: '',    
                    },
                    number: '',
                    expiration: {
                        month: '',
                        year: ''
                    },
                    pin: '',
                    csv: ''
                }
            };
        }
        
        return service;
    });
    
    app.service('SiteService', function ($resource) {
        
        var service = {
            create: create,
            fetchOne: fetchOne,
            fetchAll: fetchAll,
            update: update,
            remove: remove,
            getCurrent: getCurrent,
            setCurrent: setCurrent,
            resetCurrent: resetCurrent,
        };
        
        var current = null;
        var url = endpoint + '/site/:siteID';
        var paramDefaults = {siteID: '@siteID'};
        var userID = sessionStorage.getItem('userID');
        var params = {user: userID};
        var actions = {
            'save': {
                method: 'POST',
                withCredentials: true
            },
            'get': {
                method: 'GET',
                withCredentials: true
            },
            'getAll': {
                method: 'GET',
                params: params,
                isArray: true,
                withCredentials: true
            },
            'update': {
                method: 'PUT',
                withCredentials: true
            },
            'remove': {
                method: 'DELETE',
                withCredentials: true
            }
        };
        var resource = $resource(url, paramDefaults, actions);
        
        function create(site) {
            return resource.save(site).$promise;
        }   
        function fetchOne(siteID) {
            return resource.get({siteID: siteID}).$promise;
        }
        function fetchAll() {
            return resource.getAll().$promise;
        }
        function update(siteID, site) {
            return resource.update({siteID: siteID}, site).$promise;
        }
        function remove(siteID) {
            return resource.remove({siteID: siteID}).$promise;
        }
        function getCurrent() {
            return current;
        }
        function setCurrent(card) {
            current = card;
        }
        function resetCurrent() {
            current = {
                name: '',
                data: {
                    username: '',
                    password: '',
                    token: {
                        type: 'TOTP',
                        secret: '',
                        digits: 6,
                        period: 30,
                        counter: 0,
                        algorithm: 'SHA1',
                        enabled: false
                    }
                }
            };
        }
        
        return service;
    });
    
    app.service('PasswordGenerator', function () {
        
        var service = {
            generate: generate,
            setConfiguration: setConfiguration,
            getConfiguration: getConfiguration
        };
        
        var configuration = {
            minLength: 4,
            maxLength: 64,
            length: 16,
            useLowercase: false,
            useUppercase: true,
            useNumeric: true,
            useSpecial: false,
            useAmbiguous: false
        };
        
        function generate() {
            var pool = '';
            var password = '';
            if (configuration.useLowercase) {
                pool += 'abcdefghijkmnopqrstuvwxyz';
                if (configuration.useAmbiguous) {
                    pool += 'l';
                }
            }
            if (configuration.useUppercase) {
                pool += 'ABCDEFGHJKLMNPQRSTUVWXYZ';
                if (configuration.useAmbiguous) {
                    pool += 'IO';
                }
            }
            if (configuration.useNumeric) {
                pool += '23456789';
                if (configuration.useAmbiguous) {
                    pool += '01';
                }
            }
            if (configuration.useSpecial) {
                pool += '~`!@#$%^&*()-_+={}[]\\:;\"\'<>,.?/';
                if (configuration.useAmbiguous) {
                    pool += '|';
                }
            }
            for (var i = 0; i < configuration.length; i++) {
                var index = Math.floor(Math.random() * pool.length);
                var char = pool.charAt(index);
                password += char;
            }
            return password;
        }
        
        function setConfiguration(config) {
            configuration = config;
        }
        
        function getConfiguration() {
            return configuration;
        }
        
        return service;
    });
    
    app.service('TokenGenerator', function () {
        
        var service = {
            generate: generate
        };
        
        function generate(algorithm, secret, counter, digits) {
            var key = sjcl.codec.base32.toBits(secret);
            var count = [((counter & 0xffffffff00000000) >> 32), counter & 0xffffffff];
            switch (algorithm) {
                case 'SHA256':
                    hmacsha256 = new sjcl.misc.hmac(key, sjcl.hash.sha256);
                    var code = hmacsha256.encrypt(count);
                    break;
                case 'SHA512':
                    hmacsha512 = new sjcl.misc.hmac(key, sjcl.hash.sha512);
                    var code = hmacsha512.encrypt(count);
                    break;
                case 'SHA1':
                default:
                    hmacsha1 = new sjcl.misc.hmac(key, sjcl.hash.sha1);
                    var code = hmacsha1.encrypt(count);
                    break;
            }
            var hmacsha1 = new sjcl.misc.hmac(key, sjcl.hash.sha1);
            var code = hmacsha1.encrypt(count);
            var offset = sjcl.bitArray.extract(code, 152, 8) & 0x0f;
            var startBits = offset * 8;
            var endBits = startBits + 4 * 8;
            var slice = sjcl.bitArray.bitSlice(code, startBits, endBits);
            var dbc1 = slice[0];
            var dbc2 = dbc1 & 0x7fffffff;
            var otp = dbc2 % Math.pow(10, digits);
            var result = otp.toString();
            while (result.length < digits) {
                result = '0' + result;
            }
            return result;
        }
        
        return service;
    });
    
    app.service('EncryptionService', function () {
        
        var service = {
            encrypt: encrypt,
            decrypt: decrypt
        };
        
        var p = {
            cipher: 'aes',
            adata: '',
            iter: 1000,
            mode: 'ocb2',
            ts: 128,
            ks: 256
        };
        
        function encrypt(data) {
            var ddata = JSON.stringify(data);
            var password = sessionStorage.getItem('masterpassword');
            var params = {
                cipher: p.cipher,
                adata: p.adata,
                iter: p.iter,
                mode: p.mode,
                ts: p.ts,
                ks: p.ks
            }
            var resultparams = {};
            var edata = sjcl.encrypt(password, ddata, params, resultparams)
            console.log(resultparams);
            edata = JSON.parse(edata);
            edata = {
                salt: edata.salt,
                iv: edata.iv,
                ct: edata.ct
            }
            return edata;
        }
        
        function decrypt(data) {
            console.log(data);
            data = {
                salt: data.salt,
                iv: data.iv,
                ct: data.ct,
                cipher: p.cipher,
                adata: p.adata,
                iter: p.iter,
                mode: p.mode,
                ts: p.ts,
                ks: p.ks
            }
            console.log(data);
            var edata = JSON.stringify(data);
            var password = sessionStorage.getItem('masterpassword');
            var resultparams = {};
            var ddata = sjcl.decrypt(password, edata, {}, resultparams)
            console.log(resultparams);
            ddata = JSON.parse(ddata);
            return ddata;
        }
        
        return service;
    });

})();